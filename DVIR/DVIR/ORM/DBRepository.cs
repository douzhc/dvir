using System;
using System.Data;
using System.IO;
using SQLite;


namespace DVIR.ORM
{
   public  class DBRepository
    {
       //code to create the database

       public string CreateDB()
       {
           var output = "";
           output += "Creating Database if it doesn't exist";
           string dbPath=Path.Combine (Environment.GetFolderPath(Environment.SpecialFolder.Personal ), "BridgeHaul.db3" );
           var db = new SQLiteConnection(dbPath);
           output += "\nDatabase Created...";
           return output;
       }

       public string CreateTable()
       {
           try 
           {
               string dbPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), "BridgeHaul.db3");
               var db = new SQLiteConnection(dbPath);
               db.CreateTable<DVIRLogs>();
               string result = "Table Created Successfully..";
               return result;
           }

           catch (Exception ex)
           {
               return "Error: " + ex.Message;
           }

       }

       //Code to Insert a record
       public string InsertRecord(string driver, string date,string time, string location, string tractor,
           string trailerOne, string trailerTwo, string carrier, string address, string odometer)
       {
           try
           {
               string dbPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), "BridgeHaul.db3");
               var db = new SQLiteConnection(dbPath);
               
               DVIRLogs item = new DVIRLogs();
               item.Drivername = driver;
               item.Date = date;
               item.Time=time;
               item.Location = location;
               item.TractorNumer = tractor;
               item.TrailerOne = trailerOne;
               item.TrailerTwo = trailerTwo;
               item.CarrierName = carrier;
               item.Address = address;
               item.Odometer = odometer;
            
              
               

               db.Insert(item);

               return "Record Added";

           }
           catch(Exception ex)
           {
               return "Error: " + ex.Message;

           }

       }


       public string GetAllRecord()
       {
           string dbPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), "BridgeHaul.db3");
           var db = new SQLiteConnection(dbPath);

           string output = "";
           output += "Retreiiving the data using ORM...";
           var table = db.Table<DVIRLogs>();

           foreach (var item in table)
           {
               output += "\n" + item.Id + "----" + item.Drivername + "---" + item.Date + " ---" + item.CarrierName;
           }

           return output;

       }

       //code to retrieve specific record using ORM

       public string GetRecordbyID(int id)
       {
           string dbPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), "BridgeHaul.db3");
           var db = new SQLiteConnection(dbPath);
           var item = db.Get<DVIRLogs>(id);
           return "\n"+item.Id +"----"+ item.Drivername + "---" + item.Date + "---" + item.CarrierName;
       }

       public object GetObjectbyID(int id)
       {
           string dbPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), "BridgeHaul.db3");
           var db = new SQLiteConnection(dbPath);
           var item = db.Get<DVIRLogs>(id);
           return item;

       }
       //code to update record usng ORM

       public string updateRecord(int id, string driver, string date, string time, string location, string tractor,
           string trailerOne, string trailerTwo, string carrier, string address, string odometer)
       {
           string dbPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), "BridgeHaul.db3");
           var db = new SQLiteConnection(dbPath);

           var item = db.Get<DVIRLogs>(id);
           item.Drivername = driver;
           item.Date = date;
           item.Time = time;
           item.Location = location;
           item.TractorNumer = tractor;
           item.TrailerOne = trailerOne;
           item.TrailerTwo = trailerTwo;
           item.CarrierName = carrier;
           item.Address = address;
           item.Odometer = odometer;

           db.Update(item);

           return "Record Updated...";
       }

       //code to remove the record using ORM

       public string removeRecord(int id)
       {
           string dbPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), "BridgeHaul.db3");
           var db = new SQLiteConnection(dbPath);
           var item = db.Get<DVIRLogs>(id);
           db.Delete(item);
           return "record Deleted";

       }
    }
}