using System;
using System.Data;
using System.IO;
using SQLite;
using Android.Widget;

namespace DVIR.ORM
{
    [Table ("DVIRLogs")]
   public  class DVIRLogs
    {
        [PrimaryKey, AutoIncrement, Column("DVIR_ID")]

        public int Id { get; set; }
        [MaxLength(50)]
        public string TractorNumer { get; set; }

        public string Date { get; set; }

        public string Time { get; set; }

        public string Location { get; set; }

        public string Drivername { get; set; }

        public string TrailerOne { get; set; }

        public string TrailerTwo { get; set; }
        public string CarrierName{get; set;}

        public string Address { get; set; }

        public string Odometer { get; set; }


    }
}