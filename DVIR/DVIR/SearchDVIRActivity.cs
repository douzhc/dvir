using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using DVIR.ORM;

namespace DVIR
{
    [Activity(Label = "SearchDVIRActivity")]
    public class SearchDVIRActivity : Activity
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.SearchDVIR);
            Button btnSearch = FindViewById<Button>(Resource.Id.btnSearch);

            btnSearch.Click += btnSearch_Click;
          
        }

        void btnSearch_Click(object sender, EventArgs e)
        {
            DBRepository dbr = new DBRepository();
            EditText txtId = FindViewById<EditText>(Resource.Id.editDVIRID); 
             string result=   dbr.GetRecordbyID(int.Parse (txtId.Text ));
             Toast.MakeText(this, result, ToastLength.Short).Show();

            
        }
    }
}