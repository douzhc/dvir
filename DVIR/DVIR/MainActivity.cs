﻿using System;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using DVIR.Resources;

namespace DVIR
{
    [Activity(Label = "DVIR", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : Activity
    {
        int count = 1;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);

            Button button = FindViewById<Button>(Resource.Id.btnEditInfo);


            button.Click += delegate
            {
                var editInfo=new Intent (this, typeof (EditInfoActivity));
                StartActivity(editInfo);


            };

            // Get our button from the layout resource,
            // and attach an event to it
            //Button button = FindViewById<Button>(Resource.Id.btnEditDefect);

            //button.Click += delegate { button.Text = string.Format("{0} clicks!", count++); };
            //TextView txtView = FindViewById<TextView>(Resource.Id.txtStatus);
            //txtView.Text = "There are some defects";
        }
    }
}

