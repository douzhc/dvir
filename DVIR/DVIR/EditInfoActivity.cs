using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using DVIR.ORM;

namespace DVIR.Resources
{
    [Activity(Label = "EditInfoActivity")]
    public class EditInfoActivity : Activity
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.EditInfo);

            // Create the Database

            Button btnSaveEditInfo=FindViewById <Button >(Resource.Id.btnSaveEditInfo);
            btnSaveEditInfo.Click += btnSaveEditInfo_Click;
            // retrieve all
            Button btnRetrieve = FindViewById<Button>(Resource.Id.btnViewSaved);
            btnRetrieve.Click += btnRetrieve_Click;

            // To retrieve by ID
            

            Button btnGetbyID = FindViewById<Button>(Resource.Id.btnGetbyID);
            btnGetbyID.Click += btnGetbyID_Click;

            //update by ID

            Button btnUpdatebyID = FindViewById<Button>(Resource.Id.btnUpdate);
            btnUpdatebyID.Click += btnUpdatebyID_Click;

        }

        void btnUpdatebyID_Click(object sender, EventArgs e)
        {
            StartActivity(typeof(UpdateDVIRActivity));
        }

        void btnGetbyID_Click(object sender, EventArgs e)
        {
          StartActivity(typeof(SearchDVIRActivity));
        }

        void btnRetrieve_Click(object sender, EventArgs e)
        {
            DBRepository dbr = new DBRepository();
            var result = dbr.GetAllRecord();
            Toast.MakeText(this, result, ToastLength.Short).Show();

        }

        void btnSaveEditInfo_Click(object sender, EventArgs e)
        {
            DBRepository dbr = new DBRepository();
            var result = dbr.CreateDB();
            Toast.MakeText(this, result, ToastLength.Short).Show();


            // create the cache table
            DBRepository dbrb = new DBRepository();
            var resulttbl = dbrb.CreateTable();
            Toast.MakeText (this, result, ToastLength.Short ).Show ();
            //save record
            saveInfoFromUI();

        }

        void saveInfoFromUI()
        {
            string driverName, date, time, location, tractor, trailerOne, trailerTwo, carrier, address, odometer;
            driverName  = FindViewById<EditText>(Resource.Id.editDriverName).Text;
            date = FindViewById<EditText>(Resource.Id.editDate).Text;
            time = FindViewById<EditText>(Resource.Id.editTime).Text;
            location = FindViewById<EditText>(Resource.Id.editLocation).Text;
            tractor = FindViewById<EditText>(Resource.Id.editTractorNum).Text;
            trailerOne = FindViewById<EditText>(Resource.Id.editTrailerNumOne).Text;
            trailerTwo = FindViewById<EditText>(Resource.Id.editTrailerNumTwo).Text;
            carrier = FindViewById<EditText>(Resource.Id.editCarrier).Text;
            address = FindViewById<EditText>(Resource.Id.editAddress).Text;
            odometer = FindViewById<EditText>(Resource.Id.editOdometer).Text;

            DBRepository dbr = new DBRepository();
            string result = dbr.InsertRecord(driverName, date, time, location, tractor, trailerOne, trailerTwo, carrier, address, odometer);
            Toast.MakeText(this, result, ToastLength.Short).Show();







        }
    }
}