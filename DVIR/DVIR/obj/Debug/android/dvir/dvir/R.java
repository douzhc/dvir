/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */

package dvir.dvir;

public final class R {
    public static final class attr {
    }
    public static final class drawable {
        public static final int icon=0x7f020000;
        public static final int monoandroidsplash=0x7f020001;
    }
    public static final class id {
        public static final int btnEditDefect=0x7f060045;
        public static final int btnEditInfo=0x7f060033;
        public static final int btnGetbyID=0x7f060015;
        public static final int btnSaveDefect=0x7f060003;
        public static final int btnSaveEditInfo=0x7f060010;
        public static final int btnSearch=0x7f06004d;
        public static final int btnSign=0x7f060049;
        public static final int btnUpdate=0x7f060013;
        public static final int btnUpdateSearch=0x7f06004f;
        public static final int btnViewSaved=0x7f060032;
        public static final int checkBox1=0x7f060047;
        public static final int editAddress=0x7f06002d;
        public static final int editAircmt=0x7f06000c;
        public static final int editBrakeCmt=0x7f060009;
        public static final int editCarrier=0x7f06002a;
        public static final int editDVIRID=0x7f06004c;
        public static final int editDate=0x7f060019;
        public static final int editDriverName=0x7f060017;
        public static final int editLocation=0x7f06001e;
        public static final int editOdometer=0x7f060030;
        public static final int editText1=0x7f060012;
        public static final int editText2=0x7f060014;
        public static final int editText3=0x7f06000f;
        public static final int editTime=0x7f06001b;
        public static final int editTractorNum=0x7f060021;
        public static final int editTrailerNumOne=0x7f060024;
        public static final int editTrailerNumTwo=0x7f060027;
        public static final int scrollView1=0x7f060000;
        public static final int space1=0x7f060011;
        public static final int tableLayout1=0x7f060001;
        public static final int tableRow1=0x7f060002;
        public static final int tableRow10=0x7f060022;
        public static final int tableRow11=0x7f06001f;
        public static final int tableRow12=0x7f060028;
        public static final int tableRow13=0x7f060025;
        public static final int tableRow14=0x7f06003b;
        public static final int tableRow15=0x7f060039;
        public static final int tableRow16=0x7f060037;
        public static final int tableRow17=0x7f060046;
        public static final int tableRow2=0x7f060004;
        public static final int tableRow3=0x7f060007;
        public static final int tableRow4=0x7f06000d;
        public static final int tableRow5=0x7f06000a;
        public static final int tableRow6=0x7f060031;
        public static final int tableRow7=0x7f06002e;
        public static final int tableRow8=0x7f06002b;
        public static final int tableRow9=0x7f06001c;
        public static final int textView1=0x7f06004a;
        public static final int textView13=0x7f060016;
        public static final int textView131=0x7f060048;
        public static final int textView14=0x7f060034;
        public static final int textView15=0x7f060005;
        public static final int textView16=0x7f060006;
        public static final int textView163=0x7f06004e;
        public static final int textView2=0x7f06004b;
        public static final int textView30=0x7f060008;
        public static final int textView31=0x7f06000b;
        public static final int textView32=0x7f06000e;
        public static final int textView68=0x7f060018;
        public static final int textView69=0x7f06001a;
        public static final int textView70=0x7f06001d;
        public static final int textView71=0x7f060020;
        public static final int textView72=0x7f060023;
        public static final int textView73=0x7f060026;
        public static final int textView74=0x7f060029;
        public static final int textView75=0x7f06002c;
        public static final int textView76=0x7f06002f;
        public static final int textView82=0x7f060040;
        public static final int textView83=0x7f060042;
        public static final int txtAddress=0x7f060041;
        public static final int txtCarrier=0x7f06003f;
        public static final int txtDate=0x7f060036;
        public static final int txtDriverName=0x7f060035;
        public static final int txtLocation=0x7f06003a;
        public static final int txtOdometer=0x7f060043;
        public static final int txtStatus=0x7f060044;
        public static final int txtTime=0x7f060038;
        public static final int txtTractor=0x7f06003c;
        public static final int txtTrailerOne=0x7f06003d;
        public static final int txtTrailerTwo=0x7f06003e;
    }
    public static final class layout {
        public static final int adddefects=0x7f030000;
        public static final int editinfo=0x7f030001;
        public static final int main=0x7f030002;
        public static final int searchdvir=0x7f030003;
        public static final int updatedvir=0x7f030004;
    }
    public static final class string {
        public static final int ApplicationName=0x7f040001;
        public static final int Hello=0x7f040000;
    }
    public static final class style {
        public static final int Mono_Android_Theme_Splash=0x7f050000;
    }
}
