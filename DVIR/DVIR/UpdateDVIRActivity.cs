using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using DVIR.ORM;

namespace DVIR
{
    [Activity(Label = "UpdateDVIRActivity")]
    public class UpdateDVIRActivity : Activity
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.UpdateDVIR);

            Button btnSearch = FindViewById<Button>(Resource.Id.btnUpdateSearch);
            btnSearch.Click += btnSearch_Click;

            Button btnUpdate = FindViewById<Button>(Resource.Id.btnUpdate);
            btnUpdate.Click += btnUpdate_Click;

            //Button button = FindViewById<Button>(Resource.Id.btnEditInfo);


            //button.Click += delegate
            //{
            //    var editInfo = new Intent(this, typeof(EditInfoActivity));
            //    StartActivity(editInfo);


            //};
           
            // Create your application here
        }

        void btnUpdate_Click(object sender, EventArgs e)
        {

            UpdateInfoFromUI();
        }

        void btnSearch_Click(object sender, EventArgs e)
        {
            DBRepository dbr = new DBRepository();
            EditText txtId = FindViewById<EditText>(Resource.Id.editDVIRID);
           
            DVIRLogs  item = (DVIRLogs)dbr.GetObjectbyID(int.Parse(txtId.Text));
            EditText txtDriverName = FindViewById<EditText>(Resource.Id.editDriverName);
            EditText txtDate = FindViewById<EditText>(Resource.Id.editDate);
            EditText txtTime = FindViewById<EditText>(Resource.Id.editTime);
            EditText txtLocaton = FindViewById<EditText>(Resource.Id.editLocation);
            EditText txtTractor = FindViewById<EditText>(Resource.Id.editTractorNum);
            EditText txtTrailerOne = FindViewById<EditText>(Resource.Id.editTrailerNumOne);
            EditText txtTrailerTwo = FindViewById<EditText>(Resource.Id.editTrailerNumTwo);
            EditText txtCarrier = FindViewById<EditText>(Resource.Id.editCarrier);
            EditText txtAddress = FindViewById<EditText>(Resource.Id.editAddress);
            EditText txtOdometer = FindViewById<EditText>(Resource.Id.editOdometer);

            txtDriverName.Text = item.Drivername;
            txtDate.Text = item.Date;
            txtTime.Text = item.Time;
            txtLocaton.Text = item.Location;
            txtTractor.Text = item.TractorNumer;
            txtTrailerOne.Text = item.TrailerOne;
            txtTrailerTwo.Text = item.TrailerTwo;
            txtAddress.Text = item.Address;
            txtOdometer.Text = item.Odometer;
         
        }

        void UpdateInfoFromUI()
        {
            string driverName, date, time, location, tractor, trailerOne, trailerTwo, carrier, address, odometer;
            driverName = FindViewById<EditText>(Resource.Id.editDriverName).Text;
            date = FindViewById<EditText>(Resource.Id.editDate).Text;
            time = FindViewById<EditText>(Resource.Id.editTime).Text;
            location = FindViewById<EditText>(Resource.Id.editLocation).Text;
            tractor = FindViewById<EditText>(Resource.Id.editTractorNum).Text;
            trailerOne = FindViewById<EditText>(Resource.Id.editTrailerNumOne).Text;
            trailerTwo = FindViewById<EditText>(Resource.Id.editTrailerNumTwo).Text;
            carrier = FindViewById<EditText>(Resource.Id.editCarrier).Text;
            address = FindViewById<EditText>(Resource.Id.editAddress).Text;
            odometer = FindViewById<EditText>(Resource.Id.editOdometer).Text;

            EditText txtId = FindViewById<EditText>(Resource.Id.editDVIRID);

            DBRepository dbr = new DBRepository();
            string result = dbr.updateRecord (int.Parse (txtId.Text),driverName, date, time, location, tractor, trailerOne, trailerTwo, carrier, address, odometer);
            Toast.MakeText(this, result, ToastLength.Short).Show();







        }
    }
}